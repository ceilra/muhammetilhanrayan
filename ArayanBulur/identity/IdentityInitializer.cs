﻿using ArayanBulur.Models;
using Microsoft.AspNet.Identity;
using Microsoft.AspNet.Identity.EntityFramework;
using System;
using System.Collections.Generic;
using System.Data.Entity;
using System.Linq;
using System.Web;

namespace ArayanBulur.identity
{
    public class IdentityInitializer : CreateDatabaseIfNotExists<IdentityDataContext>
    {
        protected override void Seed(IdentityDataContext context)
        {
            //Admin roleu
            if(!context.Roles.Any(i => i.Name=="Admin"))
            {
                var store   = new RoleStore<ApplicationRole>(context);
                var manager = new RoleManager<ApplicationRole>(store);
                var role = new ApplicationRole(){ Name = "Admin",Description="adminin rolu" };
                manager.Create(role);
            }
            // User roleu
            if (!context.Roles.Any(i => i.Name == "User"))
            {
                var store = new RoleStore<ApplicationRole>(context);
                var manager = new RoleManager<ApplicationRole>(store);
                var role = new ApplicationRole() { Name= "User" ,Description=" kulancinin rolu"};
                manager.Create(role);
            }
            // bir user olusturup ve rolu vermek
            if (!context.Users.Any(i => i.Name == "Ceilra"))
            {
                var store = new UserStore<ApplicationUser>(context);
                var manager = new UserManager<ApplicationUser>(store);
                var user = new ApplicationUser() { Email= "Ceilra@arayanbulur.com" };
                manager.Create(user, "Arayanbulur7*");
                manager.AddToRole(user.Id, "Admin");
                manager.AddToRole(user.Id, "User");
            }
            if (!context.Users.Any(i => i.Name == "Ceilra1"))
            {
                var store = new UserStore<ApplicationUser>(context);
                var manager = new UserManager<ApplicationUser>(store);
                var user = new ApplicationUser() { Email = "Ceilra1@arayanbulur.com" };
                manager.Create(user, "Arayanbulur8*");
              //  manager.AddToRole(user.Id, "Admin");
                manager.AddToRole(user.Id, "User");
            }
            base.Seed(context);
        }
    }
}