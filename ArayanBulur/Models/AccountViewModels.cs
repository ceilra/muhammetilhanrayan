﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.ComponentModel.DataAnnotations;
using ArayanBulur.Resources;
namespace WebApplication1.Models
{
    public class ExternalLoginConfirmationViewModel
    {
        [Required]
        [Display(Name = "")]
        public string Email { get; set; }
    }

    public class ExternalLoginListViewModel
    {
        public string ReturnUrl { get; set; }
    }

    public class SendCodeViewModel
    {
        public string SelectedProvider { get; set; }
        public ICollection<System.Web.Mvc.SelectListItem> Providers { get; set; }
        public string ReturnUrl { get; set; }
        public bool RememberMe { get; set; }
    }

    public class VerifyCodeViewModel
    {
        [Required]
        public string Provider { get; set; }

        [Required]
        [Display(Name = "Code")]
        public string Code { get; set; }
        public string ReturnUrl { get; set; }

        [Display(Name = "Remember this browser?")]
        public bool RememberBrowser { get; set; }

        public bool RememberMe { get; set; }
    }

    public class ForgotViewModel
    {
        [Required]
        [Display(Name = "Email")]
        public string Email { get; set; }
    }

    public class LoginViewModel
    {
        [Required]
        [Display(Name = "Email", ResourceType = typeof(CeilraResource))]
        [EmailAddress]
        public string Email { get; set; }

        [Required]
        [DataType(DataType.Password)]
        [Display(Name = "Password", ResourceType = typeof(CeilraResource))]
        public string Password { get; set; }

        [Display(Name = "RememberMe", ResourceType = typeof(CeilraResource))]
        public bool RememberMe { get; set; }
    }

    public class RegisterViewModel
    {
        [Required]
        [Display(Name = "FullName", ResourceType = typeof(CeilraResource))]
        public string FullName { get; set; }
        [Required]
        [EmailAddress(ErrorMessageResourceName ="RegisterError" , ErrorMessageResourceType = typeof(CeilraResource))]
        [Display(Name = "Email", ResourceType = typeof(CeilraResource))]
        public string Email { get; set; }
        [Required]
        [StringLength(100, ErrorMessageResourceName = "passMessage", ErrorMessageResourceType = typeof(CeilraResource), MinimumLength = 6)]
        [DataType(DataType.Password, ErrorMessageResourceName = "passMessage", ErrorMessageResourceType = typeof(CeilraResource))]
        [Display(Name = "Password", ResourceType = typeof(CeilraResource))]
        public string Password { get; set; }

        [DataType(DataType.Password)]
        [Display(Name = "RePassword", ResourceType = typeof(CeilraResource))]
        [Compare("Password", ErrorMessageResourceName = "RepassError", ErrorMessageResourceType = typeof(CeilraResource))]
        public string ConfirmPassword { get; set; }
    }

    public class ResetPasswordViewModel
    {
        [Required]
        [EmailAddress]
        [Display(Name = "Email")]
        public string Email { get; set; }

        [Required]
        [StringLength(100, ErrorMessage = "The {0} must be at least {2} characters long.", MinimumLength = 6)]
        [DataType(DataType.Password)]
        [Display(Name = "Password")]
        public string Password { get; set; }

        [DataType(DataType.Password)]
        [Display(Name = "Confirm password")]
        [Compare("Password", ErrorMessage = "The password and confirmation password do not match.")]
        public string ConfirmPassword { get; set; }

        public string Code { get; set; }
    }

    public class ForgotPasswordViewModel
    {
        [Required]
        [EmailAddress]
        [Display(Name = "Email")]
        public string Email { get; set; }
    }
}
