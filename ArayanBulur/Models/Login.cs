﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Web;

namespace ArayanBulur.Models
{
    public class Login
    {
        [Required]
        [EmailAddress(ErrorMessage = "Geçerli bir e-posta adresi girmelisiniz.")]
        [DisplayName("E-Posta")]
        public string Email { get; set; }

        [Required]
        [DisplayName("Şifre")]
        public string Password { get; set; }

        [DisplayName("Beni hatırla")]
        public bool RememberMe { get; set; }
      
    }
}