namespace ArayanBulur.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class squaremeterduzenle : DbMigration
    {
        public override void Up()
        {
            AlterColumn("dbo.Product", "SquareMeter", c => c.Int());
        }
        
        public override void Down()
        {
            AlterColumn("dbo.Product", "SquareMeter", c => c.Int(nullable: false));
        }
    }
}
