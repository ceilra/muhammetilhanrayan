namespace ArayanBulur.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class namevefotoeklentisi : DbMigration
    {
        public override void Up()
        {
            CreateTable(
                "dbo.Category",
                c => new
                    {
                        CategoryID = c.Int(nullable: false, identity: true),
                        CategoryFoto = c.Binary(),
                        Name = c.String(),
                    })
                .PrimaryKey(t => t.CategoryID);
            
            CreateTable(
                "dbo.Product",
                c => new
                    {
                        ProductID = c.Int(nullable: false, identity: true),
                        CategoryID = c.Int(nullable: false),
                        ProductFoto = c.Binary(),
                        Adress = c.String(),
                        neighborhood = c.String(),
                        District = c.String(),
                        City = c.String(),
                        SquareMeter = c.Int(nullable: false),
                        RoomNumber = c.Int(),
                        Year = c.Int(),
                        ProcessorType = c.String(),
                        Ram = c.String(),
                        GraphicsCard = c.String(),
                        Resolution = c.String(),
                        Camera = c.Int(),
                        Seri = c.String(),
                        Km = c.Int(),
                        Description = c.String(),
                        Price = c.Long(nullable: false),
                        Brand = c.String(),
                        Model = c.Int(),
                        size = c.Int(),
                        height = c.Double(),
                        widht = c.Double(),
                        IsApproved = c.Boolean(nullable: false),
                        Name = c.String(),
                        Ilantarih = c.DateTime(),
                    })
                .PrimaryKey(t => t.ProductID)
                .ForeignKey("dbo.Category", t => t.CategoryID, cascadeDelete: true)
                .Index(t => t.CategoryID);
            
        }
        
        public override void Down()
        {
            DropForeignKey("dbo.Product", "CategoryID", "dbo.Category");
            DropIndex("dbo.Product", new[] { "CategoryID" });
            DropTable("dbo.Product");
            DropTable("dbo.Category");
        }
    }
}
