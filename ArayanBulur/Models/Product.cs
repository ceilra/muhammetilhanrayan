﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace ArayanBulur.Models
{
    public class Product
    {
        public int ProductID { get; set; }
        public int CategoryID { get; set; }
        public byte[] ProductFoto { get; set; }
        public string Adress { get; set; }
        public string neighborhood { get; set; }
        public string District { get; set; }
        public string City { get; set; }
        public int? SquareMeter { get; set; }
        public int? RoomNumber { get; set; }
        public int? Year { get; set; }
        public string ProcessorType { get; set; }
        public string Ram { get; set; }
        public string GraphicsCard { get; set; }
        public string Resolution { get; set; }
        public int ?Camera { get; set; }
        public string Seri { get; set; }
        public int ?Km { get; set; }
        public string Description { get; set; }
        public long Price { get; set; }
        public string Brand { get; set; }
        public int ? Model { get; set; }
        public int ?size { get; set; }
        public double ?height { get; set; }
        public double ?widht { get; set; }
        public bool IsApproved { get; set; }
        public string Name { get; set; }
        public Nullable<System.DateTime> Ilantarih { get; set; }
        public Category Categories { get; set; }
    }
}