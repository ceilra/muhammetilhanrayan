﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Web;

namespace ArayanBulur.Models
{
    public class Register
    {

        [Required]
        [DisplayName("Ad")]
        public string Name { get; set; }
        [Required]
        [DisplayName("Soyad")]
        public string SurName { get; set; }
        [Required]
        [DisplayName("Kullanici Adi")]
        public string UserName { get; set; }
        [Required]
        [EmailAddress(ErrorMessage = "Geçerli bir e-posta adresi girmelisiniz.")]
        [DisplayName("E-Posta")]
        public string Email { get; set; }
        [Required]
        [DataType(DataType.Date)]
        [DisplayFormat(DataFormatString = "{yyyy-MM-dd}", ApplyFormatInEditMode = true)]
        [DisplayName("Doğum tarihi")]
        public DateTime DateOfBirth { get; set; }
        [Required]
        [DisplayName("Şifre")]
        [DataType(DataType.Password)]
        [StringLength(30, ErrorMessage = " En az {2} karakter uzunluğunda olmalı.", MinimumLength = 6)]
        public string Password { get; set; }
        [Required]
        [DataType(DataType.Password)]
        [Compare("Password", ErrorMessage = "Şifrenizi kontrol ediniz")]
        [DisplayName("Şifre Tekrar")]
        public string RePassword { get; set; }

    }
}