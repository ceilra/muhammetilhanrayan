﻿using System;
using System.Collections.Generic;
using System.Data.Entity;
using System.Linq;
using System.Web;

namespace ArayanBulur.Models
{
    public class DataInitializer : DropCreateDatabaseIfModelChanges<CeilraContext>
    {
        protected override void Seed(CeilraContext context)
        {
            List<Category> categories = new List<Category>()
            {
                new Category(){Name="Araba" ,CategoryFoto=null},
                new Category(){Name="Gayrimenkul",CategoryFoto=null },
                new Category(){Name="Yat" ,CategoryFoto=null},
                new Category(){Name="Bilgisayar" ,CategoryFoto=null},
                new Category(){Name="Telefon",CategoryFoto=null },

            };
            foreach(var category in categories)
            {
                context.Categories.Add(category);
            }
            context.SaveChanges();
            List<Product> products = new List<Product>()
            {
                new Product(){CategoryID=1,Brand="Land Rover",Km=10000,Model=2018,Description="Eraba Eraba",Price=10000000,ProductFoto=null,IsApproved=true,Seri="Range Rover" },
                new Product(){CategoryID=2,Adress="Dr.Muhsin Alataş Cad. No 28 Daire 1",neighborhood="Agah Ateş Mahallesi",District="Körfez",City="Kocaeli",Price=1500000,Description="Ev satıyorem",ProductFoto=null,RoomNumber=3,SquareMeter=120,Year=1990,IsApproved=true },
                new Product(){CategoryID=3,Brand=" Marinboat 4.95 Samba Deluxe ",height=4.95,widht=3.95,Price=100000,Description="Marinboat 4.95 Samba Deluxe Kamaralı Tekne",ProductFoto=null,IsApproved=true },
                new Product(){CategoryID=4,Brand="MSI GF63 8RC-208XTR",GraphicsCard="GTX1050 ",ProcessorType="Intel Core i5 8300H",Ram="8GB",Resolution="FullHD",ProductFoto=null,Price=1500,Description="Kaliteli Bilgisayar",IsApproved=true },
                new Product(){CategoryID=5,Brand="Xiaomi Mi A2 LİTE",Ram="3GB",Price=1300,IsApproved=true,Description="Telefon satıyorum", }
            };
            foreach (var product in products)
            {
                context.Products.Add(product);
            }
            context.SaveChanges();


            base.Seed(context);
        }
    }
}