﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.Entity;
using System.IO;
using System.Linq;
using System.Net;
using System.Web;
using System.Web.Mvc;
using ArayanBulur.Models;

namespace ArayanBulur.Controllers
{
    [Authorize(Roles = "Admin")]
    public class AdminController : Controller
    {
        private CeilraContext db = new CeilraContext();

        // GET: Product
        public ActionResult Index()
        {
            var products = db.Products.Include(p => p.Categories);
            return View(products.ToList());
        }
      
        public ActionResult CategoryIndex()
        {
            return View(db.Categories.ToList());
        }

        public ActionResult CategoryDetails(int? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            Category category = db.Categories.Find(id);
            if (category == null)
            {
                return HttpNotFound();
            }
            return View(category);
        }

        public ActionResult CategoryCreate()
        {
            return View();
        }
        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult CategoryCreate(Category t, HttpPostedFileBase file)
        {
            try
            {
                using (CeilraContext context = new CeilraContext())
                {
                    Category _kategori = new Category();
                    if (file != null && file.ContentLength > 0)
                    {
                        MemoryStream memoryStream = file.InputStream as MemoryStream;
                        if (memoryStream == null)
                        {
                            memoryStream = new MemoryStream();
                            file.InputStream.CopyTo(memoryStream);
                        }
                        _kategori.CategoryFoto = memoryStream.ToArray();
                    }
                    _kategori.Name = t.Name;
                    context.Categories.Add(_kategori);
                    context.SaveChanges();
                    return RedirectToAction("CategoryIndex", "Admin");
                }
            }
            catch (Exception ex)
            {
                throw new Exception("Eklerken hata oluştu");
            }
        }
        public ActionResult CategoryEdit(int? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            Category category = db.Categories.Find(id);
            if (category == null)
            {
                return HttpNotFound();
            }
            return View(category);
        }
        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult CategoryEdit(Category t, HttpPostedFileBase file)
        {
            try
            {
                using (CeilraContext context = new CeilraContext())
                {
                    var _categoryDuzenle = context.Categories.Where(x => x.CategoryID == t.CategoryID).FirstOrDefault();
                    if (file != null && file.ContentLength > 0)
                    {
                        MemoryStream memoryStream = file.InputStream as MemoryStream;
                        if (memoryStream == null)
                        {
                            memoryStream = new MemoryStream();
                            file.InputStream.CopyTo(memoryStream);
                        }
                        _categoryDuzenle.CategoryFoto = memoryStream.ToArray();
                    }
                    _categoryDuzenle.Name = t.Name;

                    context.SaveChanges();
                    return RedirectToAction("CategoryIndex", "Admin");
                }
            }
            catch (Exception ex)
            {
                throw new Exception("Eklerken hata oluştu");
            }
        }

        // GET: Categories/Delete/5
        public ActionResult CategoryDelete(int? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            Category category = db.Categories.Find(id);
            if (category == null)
            {
                return HttpNotFound();
            }
            return View(category);
        }
        // POST: Categories/Delete/5
        [HttpPost, ActionName("CategoryDelete")]
        [ValidateAntiForgeryToken]
        public ActionResult CategoryDeleteConfirmed(int id)
        {
            Category category = db.Categories.Find(id);
            db.Categories.Remove(category);
            db.SaveChanges();
            return RedirectToAction("CategoryIndex");
        }

        public ActionResult ProductIndex()
        {
            var products = db.Products.Include(p => p.Categories);
            return View(products.ToList());
        }
        // GET: Product/Details/5
        public ActionResult ProductDetails(int? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            Product product = db.Products.Find(id);
            if (product == null)
            {
                return HttpNotFound();
            }
            return View(product);
        }

        // GET: Product/Create
        public ActionResult ProductCreate()
        {
            ViewBag.CategoryID = new SelectList(db.Categories, "CategoryID", "Name");
            return View();
        }

        // POST: Product/Create
        // To protect from overposting attacks, please enable the specific properties you want to bind to, for 
        // more details see https://go.microsoft.com/fwlink/?LinkId=317598.
        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult ProductCreate(Product t, HttpPostedFileBase file)
        {
            try
            {
                using (CeilraContext context = new CeilraContext())
                {
                    Product _product = new Product();
                    if (file != null && file.ContentLength > 0)
                    {
                        MemoryStream memoryStream = file.InputStream as MemoryStream;
                        if (memoryStream == null)
                        {
                            memoryStream = new MemoryStream();
                            file.InputStream.CopyTo(memoryStream);
                        }
                        _product.ProductFoto = memoryStream.ToArray();
                    }
                    _product.Adress = t.Adress;
                    _product.Brand = t.Brand;
                     _product.Camera = t.Camera;
                    _product.Categories = t.Categories;
                    _product.CategoryID = t.CategoryID;
                    _product.City = t.City;
                    _product.Description = t.Description;
                    _product.District = t.District;
                    _product.GraphicsCard = t.GraphicsCard;
                    _product.height = t.height;
                    _product.Ilantarih = t.Ilantarih;
                    _product.IsApproved = t.IsApproved;
                    _product.Km = t.Km;
                    _product.Model = t.Model;
                    _product.Name = t.Name;
                    _product.neighborhood=t.neighborhood;
                    _product.Price=t.Price;
                    _product.ProcessorType=t.ProcessorType;
                    _product.Ram=t.Ram;
                    _product.Resolution=t.Resolution;
                    _product.RoomNumber=t.RoomNumber;
                    _product.Seri=t.Seri;
                    _product.size=t.size;
                    _product.SquareMeter=t.SquareMeter;
                    _product.widht=t.widht;
                    _product.Year=t.Year;
                      
                    context.Products.Add(_product);
                    context.SaveChanges();
                    return RedirectToAction("ProductIndex", "Admin");
                }
            }
            catch (Exception ex)
            {
                throw new Exception("Eklerken hata oluştu");
            }
        }
        public ActionResult ProductEdit(int? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            Product product = db.Products.Find(id);
            if (product == null)
            {
                return HttpNotFound();
            }
            ViewBag.CategoryID = new SelectList(db.Categories, "CategoryID", "Name", product.CategoryID);
            return View(product);
        }
        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult ProductEdit(Product t, HttpPostedFileBase file)
        {
            try
            {
                using (CeilraContext context = new CeilraContext())
                {
                    var _productDuzenle = context.Products.Where(x => x.ProductID == t.ProductID).FirstOrDefault();
                    if (file != null && file.ContentLength > 0)
                    {
                       MemoryStream memoryStream = file.InputStream as MemoryStream;
                        if (memoryStream == null)
                        {
                            memoryStream = new MemoryStream();
                            file.InputStream.CopyTo(memoryStream);
                        }
                        _productDuzenle.ProductFoto = memoryStream.ToArray();
                    }
                    _productDuzenle.Adress = t.Adress;
                    _productDuzenle.Brand = t.Brand;
                    _productDuzenle.Camera = t.Camera;
                    _productDuzenle.Categories = t.Categories;
                    _productDuzenle.CategoryID = t.CategoryID;
                    _productDuzenle.City = t.City;
                    _productDuzenle.Description = t.Description;
                    _productDuzenle.District = t.District;
                    _productDuzenle.GraphicsCard = t.GraphicsCard;
                    _productDuzenle.height = t.height;
                    _productDuzenle.Ilantarih = t.Ilantarih;
                    _productDuzenle.IsApproved = t.IsApproved;
                    _productDuzenle.Km = t.Km;
                    _productDuzenle.Model = t.Model;
                    _productDuzenle.Name = t.Name;
                    _productDuzenle.neighborhood = t.neighborhood;
                    _productDuzenle.Price = t.Price;
                    _productDuzenle.ProcessorType = t.ProcessorType;
                    _productDuzenle.Ram = t.Ram;
                    _productDuzenle.Resolution = t.Resolution;
                    _productDuzenle.RoomNumber = t.RoomNumber;
                    _productDuzenle.Seri = t.Seri;
                    _productDuzenle.size = t.size;
                    _productDuzenle.SquareMeter = t.SquareMeter;
                    _productDuzenle.widht = t.widht;
                    _productDuzenle.Year = t.Year;

                    context.SaveChanges();
                    return RedirectToAction("ProductIndex", "Admin");
                }
            }
            catch (Exception ex)
            {
                throw new Exception("Eklerken hata oluştu");
            }
        }

        // POST: Product/Edit/5
        // To protect from overposting attacks, please enable the specific properties you want to bind to, for 
        // more details see https://go.microsoft.com/fwlink/?LinkId=317598.
        //[HttpPost]
        //[ValidateAntiForgeryToken]
        //public ActionResult Edit([Bind(Include = "ProductID,CategoryID,ProductFoto,Adress,neighborhood,District,City,SquareMeter,RoomNumber,Year,ProcessorType,Ram,GraphicsCard,Resolution,Camera,Seri,Km,Description,Price,Brand,Model,size,height,widht,IsApproved,Name,Ilantarih")] Product product)
        //{
        //    if (ModelState.IsValid)
        //    {
        //        db.Entry(product).State = EntityState.Modified;
        //        db.SaveChanges();
        //        return RedirectToAction("Index");
        //    }
        //    ViewBag.CategoryID = new SelectList(db.Categories, "CategoryID", "Name", product.CategoryID);
        //    return View(product);
        //}

        // GET: Product/Delete/5
        public ActionResult ProductDelete(int? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            Product product = db.Products.Find(id);
            if (product == null)
            {
                return HttpNotFound();
            }
            return View(product);
        }

        // POST: Product/Delete/5
        [HttpPost, ActionName("ProductDelete")]
        [ValidateAntiForgeryToken]
        public ActionResult ProductDeleteConfirmed(int id)
        {
            Product product = db.Products.Find(id);
            db.Products.Remove(product);
            db.SaveChanges();
            return RedirectToAction("ProductIndex");
        }

        protected override void Dispose(bool disposing)
        {
            if (disposing)
            {
                db.Dispose();
            }
            base.Dispose(disposing);
        }
    }
}
