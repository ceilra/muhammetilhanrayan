﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace ArayanBulur.Models
{
    public class Category
    { // sikinti var : RH
        public int CategoryID { get; set; }
        public byte[] CategoryFoto { get; set; }
        public string Name { get; set; }
        public List<Product> Products { get; set; }
        
    }
}