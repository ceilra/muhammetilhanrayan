﻿using System;
using System.Collections.Generic;
using System.Globalization;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using ArayanBulur.Models;

namespace WebApplication1.Controllers
{
    public class HomeController : Controller
    {
        public ActionResult Index()
        {
            using (CeilraContext context = new CeilraContext())
            {
                toViews anaSayfa = new toViews();
                anaSayfa.Products = context.Products.ToList();

                return View(anaSayfa);
            }
        }

        public ActionResult About()
        {
            ViewBag.Message = "Your application description page.";

            return View();
        }
        public ActionResult Details(int y)
        {
          
            using (CeilraContext context = new CeilraContext())
            {
                toViews anaSayfa = new toViews();

                anaSayfa.Products = context.Products.Where(x => (x.ProductID == y)).ToList();
                

                return View(anaSayfa);
            }
        }

        public ActionResult Contact()
        {
            ViewBag.Message = "Your contact page.";

            return View();
        }
        public ActionResult Araba()
        {
            ViewBag.Message = "Your Araba page.";
             using (CeilraContext context = new CeilraContext())
            {
                toViews anaSayfa = new toViews();
                anaSayfa.Products = context.Products.Where(x => (x.CategoryID == 1)).ToList();
                
                return View(anaSayfa);
            }
        }
        public ActionResult Gayrimenkul()
        {
            ViewBag.Message = "Your Gayrimenkul page.";
            using (CeilraContext context = new CeilraContext())
            {
                toViews anaSayfa = new toViews();
                anaSayfa.Products = context.Products.Where(x => (x.CategoryID == 2)).ToList();

                return View(anaSayfa);
            }
        }
         public ActionResult Yat()
        {
            ViewBag.Message = "Your Yat page.";
            using (CeilraContext context = new CeilraContext())
            {
                toViews anaSayfa = new toViews();
                anaSayfa.Products = context.Products.Where(x => (x.CategoryID ==3)).ToList();

                return View(anaSayfa);
            }
        }
         public ActionResult Bilgisayar()
        {
            ViewBag.Message = "Your Bilgisayar page.";

            using (CeilraContext context = new CeilraContext())
            {
                toViews anaSayfa = new toViews();
                anaSayfa.Products = context.Products.Where(x => (x.CategoryID == 4)).ToList();

                return View(anaSayfa);
            }
        }
         public ActionResult Telefon()
        {
            ViewBag.Message = "Your Telefon page.";

            using (CeilraContext context = new CeilraContext())
            {
                toViews anaSayfa = new toViews();
                anaSayfa.Products = context.Products.Where(x => (x.CategoryID == 5)).ToList();

                return View(anaSayfa);
            }
        }

    }
    public class toViews
    {
        public List<Product> Products { get; set; }
        public Product ProductDT { get; set; }
        public List<Category> Categories { get; set; }
    }
}