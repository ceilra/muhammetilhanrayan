﻿using Microsoft.AspNet.Identity;
using Microsoft.AspNet.Identity.EntityFramework;
using Microsoft.Owin;
using Owin;
using WebApplication1.Models;

[assembly: OwinStartupAttribute(typeof(WebApplication1.Startup))]
namespace WebApplication1
{
    public partial class Startup
    {
        ApplicationDbContext db = new ApplicationDbContext();
        public void Configuration(IAppBuilder app)
        {
            ConfigureAuth(app);

            CreatRoles();
            CreatUsers();
        }
        public void CreatUsers()
        {
            var userManager = new UserManager<ApplicationUser>(new UserStore<ApplicationUser>(db));
            var admin = new ApplicationUser();
            admin.Email = "ceilra@arayanbulur.com";
            admin.UserName = "ceilra@arayanbulur.com";
            var check = userManager.Create(admin, "arkanabakma");
            if (check.Succeeded)
            {
                userManager.AddToRole(admin.Id, "Admin");
            }
            var user = new ApplicationUser();
            user.Email = "ceilra1@arayanbulur.com";
            user.UserName = "ceilra1@arayanbulur.com";
            check = userManager.Create(user, "arkanabakma1");
            if (check.Succeeded)
            {
                userManager.AddToRole(user.Id, "User");
            }
        }
        public void CreatRoles()
        {
            var roleManager = new RoleManager<IdentityRole>(new RoleStore<IdentityRole>(db));
            IdentityRole role;
            if (!roleManager.RoleExists("Admin"))
            {
                role = new IdentityRole();
                role.Name = "Admin";
                roleManager.Create(role);
            }
         
            if (!roleManager.RoleExists("User"))
            {
                role = new IdentityRole();
                role.Name = "User";
                roleManager.Create(role);
            }
        }
    }
}
